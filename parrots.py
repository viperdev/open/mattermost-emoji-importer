#!/usr/bin/python3
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import requests
import json
from bs4 import BeautifulSoup
import re
import shutil
import time

driver = webdriver.Firefox()

url = 'http://slackmojis.com/'

driver.get(url)

#open page in selenium to get the parrot search and list all parrots
WebDriverWait(driver,5).until(EC.visibility_of_element_located((By.XPATH,'//INPUT'))).send_keys('parrot')

##toate pozele downloadate
lista_nume = [] # lista de nume ca sa le folosesc dinamic la denumiri + upload

for elem in driver.find_elements_by_xpath('//*[contains (@data-emoji-id-name, "_parrot")]'):
    link = elem.find_element_by_tag_name("img").get_attribute('src') #de aici iau download linkul
    
    nume = elem.find_element_by_xpath('(div)[last()]').text[1:][:-1] #aici scot : de la sfarsit si inceputul numelui pt ca nu e acceptat ca input pe slack
    
    lista_nume.append(nume) 
    
    #get photo via request
    response = requests.get(link, stream=True)
    with open(nume+'.gif', 'wb') as out_file:
        shutil.copyfileobj(response.raw, out_file)
    del response    

url2 = 'https://avocatoo.slack.com/customize/emoji'

driver.get(url2)
username = 'user'
password = 'pass'

driver.find_element_by_name('email').send_keys(username)
driver.find_element_by_name('password').send_keys(password)
driver.find_element_by_id('signin_btn').click()

for nume in lista_nume:
    WebDriverWait(driver,5).until(EC.visibility_of_element_located((By.ID,'emojiimg'))) 
    
    driver.find_element_by_css_selector('input[type="file"]').clear()
    driver.find_element_by_css_selector('input[type="file"]').send_keys("C:\\Users\\bognarwi\\Desktop\\parrots\\"+nume+".gif")    
    
    driver.find_element_by_id('emojiname').send_keys(nume) #nume
    driver.find_element_by_xpath("//input[@class = 'btn btn_primary']").click() #buton
    time.sleep(4) # aici exista un delay. Am testat cu 2 si 3s dar inca exista 2-3 pe care nu reuseste sa le faca.


##upload one file
### 1. press upload button
#WebDriverWait(driver,5).until(EC.visibility_of_element_located((By.ID,'emojiimg'))).click()

#driver.find_element_by_css_selector('input[type="file"]').clear()

#driver.find_element_by_css_selector('input[type="file"]').send_keys("C:\\Users\\bognarwi\\Desktop\\parrots\\975-aussie_parrot.gif")

### 2. Input name
#driver.find_element_by_id('emojiname').send_keys('aussie_parrot')
### 3. Press submit
#driver.find_element_by_xpath("//input[@class = 'btn btn_primary']").click()


